# Projet ISN

_Note: this repo was made for a french school project. As such, it is only available in french._

Ce dépôt regroupe le code écrit pour le projet annuel d'ISN (Informatique et Sciences du Numérique) d'année de Terminale, réalisé en 2015. L'objectif est de créer un quiz interactif à deux joueurs, avec suivi du score et différents thèmes de questions.

## Attributions

« 12 Polygon Backgrounds », mis à l'échelle 1600x900
Mehmet Demiray (CC-BY-NC)

## Licence

Tous droits réservés.
