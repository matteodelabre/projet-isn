'use strict';

var React = require('react');

var LoggedInMixin = require('../util/logged-in-mixin');
var getStoreListenerMixin = require('../util/store-listener-mixin');
var GameStore = require('../stores/game-store');
var GameActions = require('../actions/game-actions');

var Title = require('./standalone/title');
var Button = require('./standalone/button');
var User = require('./standalone/user');

/**
 * Calcule l'état actuel du composant à partir
 * de celui des entrepôts sur lesquels il dépend
 * 
 * @return {Object} État actuel
 */
function computeState() {
	return {
		request: GameStore.getRequest()
	};
}

/**
 * Page pour accepter ou refuser une demande de jeu
 */
var GameRequest = React.createClass({
	mixins: [LoggedInMixin, getStoreListenerMixin(GameStore, computeState)],
    answered: false,
    
    getDefaultProps: function () {
        return {
            messages: false
        };
    },
    
	getInitialState: function () {
		return computeState();
	},
    
    /**
     * Accepter la demande et commencer la partie
     */
    accept: function () {
        this.answered = true;
        GameActions.accept();
    },
    
    /**
     * Refuser la demande et revenir à l'accueil
     */
    decline: function () {
        this.answered = true;
        GameActions.decline();
    },
	
	render: function () {
        return <div>
            <Title messages={this.props.messages}
                text="Demande" onBack={this.decline} />

            <User user={this.state.request} />
            <p>{this.state.request.name} veut jouer contre vous.</p>

            <Button text="Accepter" onClick={this.accept} />
            <Button text="Refuser" onClick={this.decline} />
        </div>;
	}
});

module.exports = GameRequest;
