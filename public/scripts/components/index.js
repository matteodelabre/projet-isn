'use strict';

var React = require('react');

var UserActions = require('../actions/user-actions');
var GameActions = require('../actions/game-actions');

var Title = require('./standalone/title');
var Avatar = require('./standalone/avatar');
var User = require('./standalone/user');
var Game = require('./standalone/game');
var Button = require('./standalone/button');

var router = require('../router');

/**
 * Page principale
 */
var Index = React.createClass({
    getDefaultProps: function () {
        return {
            user: false,
            users: [],
            messages: false
        };
    },
    
    /**
     * Crée une fonction qui, lorsqu'elle est appelée,
     * envoie une demande de jeu à l'utilisateur donné
     *
     * @param {string} id Identifiant de l'autre utilisateur
     * @return {null}
     */
    request: function (id) {
        return function () {
            GameActions.request(id);
        };
    },
    
    /**
     * Crée une fonction qui, lorsqu'elle est appelée,
     * reprend la partie donnée
     *
     * @param {string} id Identifiant de prtie
     * @return {null}
     */
    play: function (id) {
        return function () {
            router.transitionTo('game', {
                id: id
            });
        };
    },
    
	/**
	 * Se déconnecte du compte
	 */
	logout: function () {
		UserActions.logout();
	},
	
	render: function () {
		var value, id,
            users, userList,
            games, gameList;
        
		if (this.props.user) {
            games = this.props.games;
            users = this.props.users;
            gameList = [];
            userList = [];
            
            // on s'occupe de la liste des parties
            for (id in games) {
                if (games.hasOwnProperty(id) && !games[id].finished) {
                    gameList.push(<Game key={id} game={games[id]}
                                        onClick={this.play(id)} />);
                }
            }
            
            if (!gameList.length) {
                gameList.push(<p key="no-game">
                    Vous n'avez aucune partie en cours.
                    Cliquez sur un adversaire ci-dessous
                    pour en créer une nouvelle.
                </p>);
            }
            
            // création de la liste d'utilisateurs connectés
            for (id in users) {
                if (users.hasOwnProperty(id)) {
                    userList.push(<User key={id} user={users[id]}
                                        onClick={this.request(id)} />);
                }
            }
            
            if (!userList.length) {
                userList.push(<p key="no-opponent">
                    Aucun adversaire n'est disponible pour
                    le moment, désolé !
                </p>);
            }
            
			value = <div>
                <Title extend={false} showBack={false}
                       messages={this.props.messages}
                       text={<span>
                        <Avatar url={this.props.user.avatar} size={60} />
                        <span style={{verticalAlign: 'middle'}}>
                            {this.props.user.name}
                        </span>
                    </span>} />
                
                <p>
                    Vous avez {this.props.user.score}
                    {this.props.user.score === 1 ? ' point' : ' points'}.
                </p>
                
                <h2>Parties en cours</h2>
                {gameList}
                
                <h2>Adversaires disponibles</h2>
                {userList}
                
				<Button text="Se déconnecter" onClick={this.logout} />
			</div>;
		} else {
			value = <div>
                <Title extend={false} showBack={false}
                       text="Quiz" messages={this.props.messages} />
                
				<Button link="login" text="Se connecter" />
				<Button link="register" text="S'inscrire" />
			</div>;
		}
		
		return value;
	}
});

module.exports = Index;
