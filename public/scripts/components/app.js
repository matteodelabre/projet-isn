'use strict';

var React = require('react');
var Radium = require('radium');
var Router = require('react-router');
var RouteHandler = Router.RouteHandler;

var getStoreListenerMixin = require('../util/store-listener-mixin');
var UserStore = require('../stores/user-store');
var MessageStore = require('../stores/message-store');
var LoadingStore = require('../stores/loading-store');
var GameStore = require('../stores/game-store');

var Message = require('./standalone/message');
var Spinner = require('./standalone/spinner');

var styles = {
	background: {
		width: '100%',
		height: '100%',
        overflow: 'auto',
		
		background: 'url("/images/background.jpg") center center #282c33',
		backgroundSize: 'cover'
	},
	
	wrapper: {
        '@media (max-width: 520px)': {
            width: '100%',
            padding: '0 10px'
        },
        
        '@media (min-width: 521px)': {
            width: 500,
            margin: 'auto'
        },
        
		fontFamily: 'Roboto, sans-serif',
        fontSize: '1.1em',
		color: 'white'
	}
};

/**
 * Calcule l'état actuel du composant à partir
 * de celui des entrepôts sur lesquels il dépend
 * 
 * @return {Object} État actuel
 */
function computeState() {
	return {
		user: UserStore.getUser(),
		users: UserStore.getUsers(),
        games: GameStore.getGames(),
		messages: MessageStore.getMessages(),
        loading: LoadingStore.getLoading()
	};
}

/**
 * Gère la structure commune à toutes les pages
 */
var App = React.createClass(Radium.wrap({
	mixins: [getStoreListenerMixin([
        UserStore, MessageStore, LoadingStore, GameStore
    ], computeState)],
    
	getInitialState: function () {
		return computeState();
	},
	
	render: function () {
		var messages, messageList = [],
            users, user, games, id, content;
        
        users = this.state.users;
		user = this.state.user;
        games = this.state.games;
        
        if (this.state.loading) {
            content = <Spinner message={this.state.loading} />;
        } else {
            // récupération des messages de succès/d'erreur
            messages = this.state.messages;
            
            for (id in messages) {
                if (messages.hasOwnProperty(id)) {
                    messageList.push(<Message key={id} {...messages[id]} />);
                }
            }
            
            // appel du composant correspondant à la route actuelle
            content = <RouteHandler user={user} users={users}
                                    games={games} messages={messageList} />;
        }
		
		return <div style={styles.background}>
			<div style={styles.wrapper}>
                {content}
			</div>
		</div>;
	}
}));

module.exports = App;
