'use strict';

var React = require('react');
var Radium = require('radium');

var themes = require('../../../data/themes');
var LoggedInMixin = require('../util/logged-in-mixin');
var GameActions = require('../actions/game-actions');

var Title = require('./standalone/title');
var Question = require('./standalone/question');

var router = require('../router');

var styles = {
    tiles: {
        padding: 0,
        listStyle: 'none'
    },
    
    tile: {
        position: 'relative',
        
        background: 'center center no-repeat',
        backgroundSize: 'cover',
        height: 125,
        width: '100%',
        
        color: 'white',
        lineHeight: '125px',
        textAlign: 'center',
        fontSize: '1.3em',
        
        cursor: 'pointer'
    },
    
    inner: {
        display: 'inline-block',
        position: 'absolute',
        top: 0,
        left: 0,
        
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, 0.2)'
    }
};

/**
 * Affiche l'interface de jeu dans une partie
 */
var GamePlay = React.createClass(Radium.wrap({
    mixins: [LoggedInMixin],
    getDefaultProps: function () {
        return {
            messages: false,
            user: false,
            games: {}
        };
    },
    
    /**
     * Récupère la partie correspondante à l'URL
     *
     * @return {Object|false} Partie correspondante
     */
    getGame: function () {
        return this.props.games[this.props.params.id] || false;
    },
    
    /**
     * Récupère la liste des thèmes disponibles
     *
     * @return {Array} Liste de thèmes
     */
    getThemes: function () {
        var gameThemes = this.getGame().themes,
            themeIds = Object.keys(themes),
            playedIds = [];
        
        playedIds = gameThemes.map(function (gameTheme) {
            return gameTheme.themeId;
        });
        
        return themeIds.filter(function (id) {
            return playedIds.indexOf(id) === -1;
        });
    },
    
    /**
     * Récupère le dernier thème non-joué
     *
     * @return {Object|false} Dernier thème ou false
     */
    getLastTheme: function () {
        var themesList = this.getGame().themes,
            last = themesList[themesList.length - 1];
        
        if (!last || last.played) {
            return false;
        }
        
        return last;
    },
    
    /**
     * Récupère la première question non-jouée du thème donné
     *
     * @param {Object} theme Thème à vérifier
     * @return {Object|false} Question non-jouée ou "false" si toutes le sont
     */
    getThemeFirstQuestion: function (theme) {
        var questions = theme.questions, length = questions.length, i;
        
        for (i = 0; i < length; i += 1) {
            if (!questions[i].played) {
                return {
                    number: i,
                    id: questions[i].questionId
                };
            }
        }
        
        return false;
    },
    
    /**
     * Crée une fonction qui, lorsqu'elle est appelée,
     * choisit le thème donné et récupère les questions associées
     *
     * @param {string} id Identifiant du thème à choisir
     * @return {null}
     */
    select: function (id) {
        return function () {
            GameActions.select(this.props.params.id, id);
        }.bind(this);
    },
    
    /**
     * Envoie la réponse à la question courante
     *
     * @param {number} i Indice de la réponse
     * @return {null}
     */
    answer: function (i) {
        GameActions.answer(this.props.params.id, i);
    },
    
    /**
     * Si on perd la main, retour à l'écran principal
     */
    componentWillUpdate: function (nextProps) {
        var game = nextProps.games[this.props.params.id];
        
        if (!game || !game.playing) {
            router.transitionTo('game', {
                id: this.props.params.id
            });
        }
    },
    
	render: function () {
        var theme = this.getLastTheme(),
            question, tiles, number;
        
        // on doit d'abord choisir le thème de la série
        // de questions, afficher les thèmes restants
        if (!theme) {
            tiles = this.getThemes().map(function (id) {
                var name = themes[id].name,
                    url = '/images/themes/' + id + '.jpg';
                
                return <li style={[
                    styles.tile,
                    {backgroundImage: 'url(' + url + ')'}
                ]} key={id} onClick={this.select(id)}>
                    <span style={styles.inner}>{name}</span>
                </li>;
            }, this);
            
            return <div>
                <Title text="Choisir un thème"
                       messages={this.props.messages} />
                
                <ul style={styles.tiles}>{tiles}</ul>
            </div>;
        }
        
        // on doit répondre à une question de la série de questions
        question = this.getThemeFirstQuestion(theme) || {};
        
		return <div>
            <Title text="Jouer" messages={this.props.messages} />
            <Question themeId={theme.themeId} number={question.number}
                      questionId={question.id} answer={this.answer} />
        </div>;
	}
}));

module.exports = GamePlay;
