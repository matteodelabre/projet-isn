'use strict';

var React = require('react');

var UserActions = require('../actions/user-actions');

var Title = require('./standalone/title');
var Input = require('./standalone/input');
var Button = require('./standalone/button');

/**
 * Formulaire d'inscription
 */
var Register = React.createClass({
    getDefaultProps: function () {
        return {
            messages: false
        };
    },

    /**
     * Crée un compte avec les données fournies
     * 
     * @param {Event} e Événément de soumission du formulaire
     */
    register: function (e) {
        e.preventDefault();
        UserActions.register(
            this.refs.name.state.value,
            this.refs.email.state.value,
            this.refs.password.state.value
        );
    },

    render: function () {
        return <form onSubmit={this.register}>
            <Title text="S'inscrire" messages={this.props.messages} />

            <Input ref="name" placeholder="Nom d'utilisateur" />
            <Input ref="email" type="email"
                       placeholder="Adresse mail" />
            <Input ref="password" type="password"
                       placeholder="Mot de passe" />

            <Button text="Inscription" />
        </form>;
    }
});

module.exports = Register;
