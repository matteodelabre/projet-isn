'use strict';

var React = require('react');

var UserActions = require('../actions/user-actions');

var Title = require('./standalone/title');
var Input = require('./standalone/input');
var Button = require('./standalone/button');

/**
 * Formulaire de connexion
 */
var Login = React.createClass({
    getDefaultProps: function () {
        return {
            messages: false
        };
    },
    
	/**
	 * Se connecte avec les identifiants donnés
	 * 
	 * @param {Event} e Événément de soumission du formulaire
	 */
	login: function (e) {
		e.preventDefault();
		UserActions.login(
			this.refs.name.state.value,
			this.refs.password.state.value
		);
	},
	
	render: function () {
		return <form onSubmit={this.login}>
            <Title text="Se connecter" messages={this.props.messages} />
            
			<Input ref="name" placeholder="Nom d'utilisateur" />
			<Input ref="password" type="password"
                       placeholder="Mot de passe" />
			
			<Button text="Connexion" />
		</form>;
	}
});

module.exports = Login;
