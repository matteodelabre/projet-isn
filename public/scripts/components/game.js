'use strict';

var React = require('react');
var Radium = require('radium');
var moment = require('moment');
require('moment/locale/fr');

var LoggedInMixin = require('../util/logged-in-mixin');

var Title = require('./standalone/title');
var User = require('./standalone/user');
var Button = require('./standalone/button');

var router = require('../router');
var commonStyles = require('../util/common-styles');

var styles = {
    wrapper: {
        '@media (min-width: 450px)': {
            display: 'table',
            width: '100%'
        }
    },
    
    half: {
        '@media (min-width: 450px)': {
            display: 'table-cell',
            verticalAlign: 'middle',
            width: '50%'
        }
    },
    
    table: {
        borderCollapse: 'collapse',
        border: '2px solid white',
        
        width: '100%',
        textAlign: 'center',
        margin: '1em 0'
    },
    
    cell: {
        padding: '6px',
        width: '50%'
    },
    
    leftCell: {
        borderRight: '1px solid white'
    },
    
    headingCell: {
        borderBottom: '1px solid white',
        borderTop: '1px solid white',
        width: '100%'
    },
    
    correct: {
        backgroundColor: commonStyles.colors.green
    },
    
    wrong: {
        backgroundColor: commonStyles.colors.red
    }
};

/**
 * Affiche une partie
 */
var Game = React.createClass(Radium.wrap({
    mixins: [LoggedInMixin],
    getDefaultProps: function () {
        return {
            messages: false,
            user: false,
            games: {}
        };
    },
    
    /**
     * Fabrique une cellule rendant compte de l'état d'une question
     * (non-répondu, répondu faux ou répondu juste)
     *
     * @param {number} id Indice dans la boucle
     * @param {boolean} left Si la cellule est dans la colonne de gauche
     * @param {boolean} played Indique si l'on a répondu à cette question
     * @param {boolean} correct Indique si l'on a répondu juste
     * @return {ReactElement} Cellule générée
     */
    makeCell: function (id, left, played, correct) {
        return <td style={[
            styles.cell,
            left && styles.leftCell,
            played && correct && styles.correct,
            played && !correct && styles.wrong
        ]}>Q{id + 1}</td>;
    },
    
    /**
     * Récupère la partie correspondante à l'URL
     *
     * @return {Object|false} Partie correspondante
     */
    getGame: function () {
        return this.props.games[this.props.params.id] || false;
    },
    
    /**
     * Joue dans la partie associée au composant
     */
    play: function () {
        router.transitionTo('game-play', {
            id: this.props.params.id
        });
    },
    
	render: function () {
        var game = this.getGame(), button, align,
            themes, questions, themesLength, questionsLength,
            i, j, themeTable, themeRows = [];
        
        if (window.matchMedia('(max-width: 450px)').matches) {
            align = 'left';
        } else {
            align = 'right';
        }
        
        if (!game.finished) {
            if (game.playing) {
                button = <Button text="Jouer" onClick={this.play} />;
            } else {
                button = <Button text={game.opponent.name + ' joue…'} />;
            }
        }
        
        // génération du tableau des résultats
        if (game.themes.length) {
            themes = game.themes;
            themesLength = themes.length;
            
            themeRows.push(<tr key="head">
                <td style={[styles.cell, styles.leftCell]}>Vous</td>
                <td style={styles.cell}>{game.opponent.name}</td>
            </tr>);
            
            for (i = 0; i < themesLength; i += 1) {
                themeRows.push(<tr key={'theme-' + i + '-head'}>
                    <th colSpan="2" style={[
                        styles.cell,
                        styles.headingCell
                    ]}>{'Thème ' + (i + 1)}</th>
                </tr>);
                
                questions = themes[i].questions;
                questionsLength = questions.length;
                
                for (j = 0; j < questionsLength; j += 1) {
                    themeRows.push(<tr key={'theme-' + i + '-q-' + j}>
                        {this.makeCell(
                            j,
                            true,
                            questions[j].played,
                            questions[j].correct
                        )}
                        {this.makeCell(
                            j,
                            false,
                            questions[j].oppPlayed,
                            questions[j].oppCorrect
                        )}
                    </tr>);
                }
            }
            
            themeTable = <table key="themes" style={styles.table}>
                {themeRows}
            </table>;
        }
        
		return <div>
            <Title text={'Jeu contre ' + game.opponent.name}
                   messages={this.props.messages} />
            
            <div style={styles.wrapper}>
                <div key="left" style={styles.half}>
                    <User user={this.props.user} score={game.score} />
                </div>

                <div key="right" style={styles.half}>
                    <User user={game.opponent} score={game.oppScore}
                          align={align} />
                </div>
            </div>
            
            <span>
                {game.finished ? 'Partie terminée. Commencée ' : 'Commencé '}
                {moment(game.startTime).fromNow()}
            </span>
            
            {themeTable}
            {button}
        </div>;
	}
}));

module.exports = Game;
