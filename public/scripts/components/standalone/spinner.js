'use strict';

var React = require('react');
var Radium = require('radium');
var objectAssign = require('object-assign');

var color = 'white';
var styles = {
	spinner: {
        position: 'relative',
        margin: '1em auto',
        
        border: '6px solid ' + color,
        borderRadius: 24,
        
        width: 48,
        height: 48
    },
    
    needle: {
        position: 'absolute',
        
        animation: 'spinner 1250ms infinite linear',
        transformOrigin: '3px 3px',
        
        background: color,
        borderRadius: 3,
        
        width: 6,
        height: 19.2,
        left: 15,
        top: 15
    },
    
    message: {
        textAlign: 'center'
    }
};

styles.large = objectAssign({}, styles.needle, {
    animation: 'spinner 1250ms infinite linear',
    height: 19.2
});

styles.short = objectAssign({}, styles.needle, {
    animation: 'spinner 15000ms infinite linear',
    height: 16
});

/**
 * Affiche l'élément de progression
 */
var Spinner = React.createClass(Radium.wrap({
	getDefaultProps: function () {
		return {
			message: ''
		};
	},
	
	render: function () {
		return <figure>
            <div style={styles.spinner}>
                <div style={styles.large}></div>
                <div style={styles.short}></div>
            </div>
            
            <figcaption style={styles.message}>
                {this.props.message}
            </figcaption>
        </figure>;
	}
}));

module.exports = Spinner;
