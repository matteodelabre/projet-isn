'use strict';

var React = require('react');
var Radium = require('radium');
var objectAssign = require('object-assign');

var commonStyles = require('../../util/common-styles');

var styles = objectAssign({}, commonStyles.formInputs, {
	':focus': {
		outline: 'none',
		background: '#EAEAEA'
	}
});

/**
 * Affiche une entrée
 */
var Input = React.createClass(Radium.wrap({
	getDefaultProps: function () {
		return {
			type: 'text',
			block: true,
			placeholder: '',
			initialValue: '',
			onChange: function () {}
		};
	},
	
	getInitialState: function () {
		return {
			value: this.props.initialValue
		};
	},
	
	onChange: function (e) {
		this.setState({
			value: e.target.value
		}, function () {
			this.props.onChange(this.state.value);
		}.bind(this));
	},
	
	render: function () {
		var input = <input type={this.props.type} value={this.state.value}
					       placeholder={this.props.placeholder}
					       onChange={this.onChange}
						   style={styles} />
		
		if (this.props.block) {
			input = <p style={commonStyles.paragraphs}>{input}</p>;
		}
		
		return input;
	}
}));

module.exports = Input;
