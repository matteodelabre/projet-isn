'use strict';

var React = require('react');
var Radium = require('radium');
var objectAssign = require('object-assign');

var MessageActions = require('../../actions/message-actions');
var commonStyles = require('../../util/common-styles');

var styles = {
	base: objectAssign({}, commonStyles.paragraphs, {
		padding: 0,
        overflow: 'hidden',
		boxShadow: commonStyles.shadow,
		cursor: 'pointer'
	}),
	
	success: {
		background: commonStyles.colors.green
	},
	
	fail: {
		background: commonStyles.colors.red
	},
    
    textPart: {
        display: 'table-cell',
        
        height: '100%',
        padding: '8px 0 8px 12px'
    },
	
	closePart: {
        display: 'table-cell',
        verticalAlign: 'middle',
        textAlign: 'center',
        
        width: 42,
        height: '100%',
		
		fontSize: '1.2em'
	}
};

/**
 * Affiche un message de la liste
 */
var Message = React.createClass(Radium.wrap({
	getDefaultProps: function () {
		return {
			id: 0,
			kind: 'fail',
			text: ''
		};
	},
	
	/**
	 * Supprime ce message de la liste des messages
	 */
	remove: function () {
		MessageActions.remove(this.props.id);
	},
	
	render: function () {
		return <p style={[
			styles.base,
			this.props.kind === 'success' && styles.success,
			this.props.kind === 'fail' && styles.fail
		]} onClick={this.remove}>
            <span style={{display: 'table', width: '100%'}}>
                <span style={styles.textPart}>{this.props.text}</span>
                <span style={styles.closePart}>
                    &times;
                </span>
            </span>
		</p>;
	}
}));

module.exports = Message;
