'use strict';

var React = require('react');
var Radium = require('radium');

var Avatar = require('./avatar');
var styles = require('../../util/common-styles').info;

/**
 * Affiche les informations sur une partie
 */
var Game = React.createClass(Radium.wrap({
    getDefaultProps: function () {
        return {
            game: {},
            onClick: function () {}
        };
    },
    
    render: function () {
        return <div style={[
            styles.base,
            this.props.onClick && {cursor: 'pointer'}
        ]} onClick={this.props.onClick}>
            <Avatar url={this.props.game.opponent.avatar} size={50}
                    style={styles.avatar} />
            
            <span style={styles.aside}>
                Partie contre <span style={{fontWeight: 'bold'}}>
                    {this.props.game.opponent.name}
                </span><br />
                {this.props.game.playing ?
                    'À vous de jouer !' : "En attente de l'adversaire"}
            </span>
        </div>;
    }
}));

module.exports = Game;
