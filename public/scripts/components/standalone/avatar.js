'use strict';

var React = require('react');
var Radium = require('radium');
var md5 = require('MD5');

var styles = {
    base: {
        borderRadius: '100%',
        marginRight: 15,
        verticalAlign: 'middle'
    },
    
    right: {
        float: 'right',
        marginRight: 0,
        marginLeft: 15
    }
};

/**
 * Affiche l'avatar d'un utilisateur
 */
var Avatar = React.createClass(Radium.wrap({
	getDefaultProps: function () {
		return {
			url: '',
			email: '',
			size: 80,
            align: 'left'
		};
	},
	
	/**
	 * Récupère l'adresse de l'avatar si fournie, ou
	 * la calcule à partir de l'adresse mail
	 * 
	 * @return {string} Lien de l'avatar
	 */
	getUrl: function () {
		if (this.props.url) {
			return this.props.url;
		}
		
		return 'https://secure.gravatar.com/' +
			md5(this.props.email) + '?d=mm';
	},
	
	render: function () {
		return <img src={this.getUrl()} alt="Avatar"
					width={this.props.size}
					height={this.props.size}
                    style={[
                        styles.base,
                        this.props.align === 'right' && styles.right
                    ]} />;
	}
}));

module.exports = Avatar;
