'use strict';

var React = require('react');
var Radium = require('radium');
var Link = require('react-router').Link;
var objectAssign = require('object-assign');

var commonStyles = require('../../util/common-styles');

var styles = objectAssign({}, commonStyles.formInputs, {
    border: '2px solid white',
	
    color: 'white',
	textDecoration: 'none',
	textAlign: 'center',
	
    background: 'none',
    boxShadow: commonStyles.shadow,
	
    cursor: 'pointer'
});

/**
 * Affiche un bouton
 */
var Button = React.createClass(Radium.wrap({
	getDefaultProps: function () {
		return {
			text: 'Envoyer',
			link: false,
			
			block: true,
			onClick: function () {}
		};
	},
	
	render: function () {
		var button;
		
		if (!this.props.link) {
			button = <button style={styles} onClick={this.props.onClick}>
				{this.props.text}
			</button>;
		} else {
			button = <Link style={styles} onClick={this.props.onClick}
						   to={this.props.link}>
				{this.props.text}
			</Link>;
		}
		
		if (this.props.block) {
			button = <p style={commonStyles.paragraphs}>{button}</p>;
		}
		
		return button;
	}
}));

module.exports = Button;
