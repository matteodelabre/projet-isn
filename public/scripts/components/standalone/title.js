'use strict';

var React = require('react');
var Radium = require('radium');

var router = require('../../router');

var styles = {
    base: {
        fontSize: '2.2em',
        whiteSpace: 'nowrap',
        
        textOverflow: 'ellipsis',
        overflow: 'hidden'
    },
    
    back: {
        display: 'inline-block',
        
        border: '2px solid white',
        borderRadius: '100%',
        
        fontSize: 20,
        fontWeight: 'bold',
        
        width: 42,
        height: 42,
        marginRight: '0.75em',
        verticalAlign: 'middle',
        
        textAlign: 'center',
        lineHeight: '38px',
        
        cursor: 'pointer'
    }
};

/**
 * Affiche le titre principal d'une application
 */
var Title = React.createClass(Radium.wrap({
    getDefaultProps: function () {
        return {
            text: 'Titre',
            extend: true,
            
            showBack: true,
            onBack: function () {
                if (!router.goBack()) {
                    router.transitionTo('/');
                }
            },
            
            messages: false
        };
    },
    
    render: function () {
        var back;
        
        if (this.props.extend) {
            document.title = 'Quiz – ' + this.props.text;
        } else {
            document.title = 'Quiz';
        }
        
        if (this.props.showBack) {
            back = <span onClick={this.props.onBack} style={styles.back}>
                ←
            </span>;
        }
        
        return <div>
            <h1 style={styles.base}>
                {back}
                
                <span style={{verticalAlign: 'middle'}}>
                    {this.props.text}
                </span>
            </h1>
            
            {this.props.messages}
        </div>;
    }
}));

module.exports = Title;
