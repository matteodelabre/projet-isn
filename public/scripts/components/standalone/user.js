'use strict';

var React = require('react');
var Radium = require('radium');

var Avatar = require('./avatar');
var styles = require('../../util/common-styles').info;

/**
 * Affiche les informations sur un utilisateur
 */
var User = React.createClass(Radium.wrap({
    getDefaultProps: function () {
        return {
            user: {},
            score: null,
            align: 'left',
            
            onClick: function () {}
        };
    },
    
    render: function () {
        var score = (this.props.score === null) ?
            this.props.user.score : this.props.score;
        
        return <div style={[
            styles.base,
            this.props.onClick && {cursor: 'pointer'},
            this.props.align === 'right' && {textAlign: 'right'}
        ]} onClick={this.props.onClick}>
            <Avatar key="avatar" url={this.props.user.avatar} size={50}
                    align={this.props.align} />
            
            <span style={styles.aside}>
                <span style={{fontWeight: 'bold'}}>
                    {this.props.user.name}
                </span>
                <br />
                
                <span>
                    Score : {score}
                    {score === 1 ? ' point' : ' points'}
                </span>
            </span>
        </div>;
    }
}));

module.exports = User;
