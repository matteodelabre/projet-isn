'use strict';

var React = require('react');
var Radium = require('radium');

var themes = require('../../../../data/themes');

var styles = {
    wrapper: {
        padding: 0,
        listStyle: 'none'
    },
    
    answer: {
        display: 'block',
        background: 'rgba(0, 0, 0, 0.2)',
        padding: '12px 20px',
        fontSize: '0.9em',
        margin: '0.5em 0',
        
        cursor: 'pointer'
    }
};

/**
 * Affiche l'interface pour répondre à une question
 */
var User = React.createClass(Radium.wrap({
    getDefaultProps: function () {
        return {
            number: 0,
            themeId: false,
            questionId: false,
            
            answer: function () {}
        };
    },
    
    /**
     * Crée une fonction qui, lorsqu'elle est appelée,
     * appelle la fonction de rappel pour soumettre la réponse
     *
     * @param {number} i Indice de la réponse
     * @return {null}
     */
    answer: function (i) {
        return function () {
            this.props.answer(i);
        }.bind(this);
    },
    
    render: function () {
        var theme = themes[this.props.themeId], question, answers;
        
        if (!theme) {
            return false;
        }
        
        question = theme.questions[this.props.questionId];
        
        if (!question) {
            return false;
        }
        
        answers = question.answers.map(function (answer, i) {
            return <li style={styles.answer} onClick={this.answer(i)}
                       key={'answer-' + i}>
                {i + 1}. {answer}
            </li>;
        }, this);
        
        return <div>
            <h2>{theme.name} (question {this.props.number + 1}/3)</h2>
            
            <p>{question.question}</p>
            <ul style={styles.wrapper}>{answers}</ul>
        </div>;
    }
}));

module.exports = User;
