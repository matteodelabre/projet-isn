'use strict';

/**
 * Point d'entrée du client
 *
 * Charge React et le routeur puis affiche la vie principale
 */

var React = require('react');
var router = require('./router');

router.run(function (Root) {
	React.render(<Root />, document.body);
});
