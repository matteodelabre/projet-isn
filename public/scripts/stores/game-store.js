'use strict';

var objectAssign = require('object-assign');

var AppDispatcher = require('../dispatcher');
var Store = require('../util/store');
var UserStore = require('../stores/user-store');
var ActionTypes = require('../../../data/constants').ActionTypes;

var router = require('../router');

var games = {};
var requestId = null;

/**
 * GameStore
 * 
 * Enregistre les informations relatives au jeu en cours
 */
var GameStore = objectAssign({}, Store.prototype, {
    /**
     * Récupère l'adversaire ayant effectué une demande de jeu
     * 
     * @return {Object|false} Adversaire ou "false"
     */
    getRequest: function () {
        return UserStore.getUsers()[requestId] || false;
    },
    
    /**
     * Récupère la liste des parties pour cet utilisateur
     *
     * @return {Object} Liste des parties
     */
    getGames: function () {
        return games;
    },
    
    /**
     * Récupère la partie ayant l'ID donné
     *
     * @return {Object} Partie correspondante ou "false"
     */
    getGame: function (id) {
        return games[id] || false;
    }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        // l'utilisateur s'est connecté
        case ActionTypes.USER_LOGIN:
            games = action.games;
            GameStore.emitChange();
            break;
        
        // mise à jour d'une partie existante
        case ActionTypes.GAME_UPDATE:
            if (games[action.game.id]) {
                games[action.game.id] = action.game;
                GameStore.emitChange();
            }
            break;
        
        // création d'une nouvelle partie
        case ActionTypes.GAME_ADD:
            games[action.game.id] = action.game;
            router.transitionTo('game', {
                id: action.game.id
            });
            
            GameStore.emitChange();
            break;
        
        // réception d'une demande de nouvelle partie
        case ActionTypes.GAME_REQUEST:
            requestId = action.id;
            router.transitionTo('game-request');
            
            GameStore.emitChange();
            break;
        
        // refus d'une demande de nouvelle partie
        case ActionTypes.GAME_REQUEST_DECLINE:
            requestId = null;
            router.transitionTo('/');
            
            GameStore.emitChange();
            break;
    }
});

module.exports = GameStore;
