'use strict';

var objectAssign = require('object-assign');

var AppDispatcher = require('../dispatcher');
var Store = require('../util/store');
var ActionTypes = require('../../../data/constants').ActionTypes;

var user = false;
var users = {};

/**
 * UserStore
 * 
 * Gestion de l'état de connexion de l'utilisateur
 */
var UserStore = objectAssign({}, Store.prototype, {
    /**
     * Récupère l'utilisateur actuel
     * 
     * @return {Object|false} Utilisateur actuel ou "false" s'il n'y en a pas
     */
    getUser: function () {
        return user;
    },

    /**
     * Récupère la liste des autres utilisateurs si
     * l'utilisateur est actuellement connecté
     * 
     * @return {Array<Object>|false} Autres utilisateurs ou "false" si non-co
     */
     getUsers: function () {
         return user && users;
     }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        // l'utilisateur s'est connecté
        case ActionTypes.USER_LOGIN:
            user = action.user;
            users = action.users;

            UserStore.emitChange();
            break;

        // l'utilisateur s'est déconnecté
        case ActionTypes.USER_LOGOUT:
            user = false;
            users = {};

            UserStore.emitChange();
            break;
        
        // ajoute (localement) un score à l'utilisateur
        case ActionTypes.USER_ADD_SCORE:
            if (user.id === action.id) {
                user.score += action.score;
                UserStore.emitChange();
            }
            
            if (users[action.id]) {
                users[action.id].score += action.score;
                UserStore.emitChange();
            }
            break;

        // un utilisateur est arrivé
        case ActionTypes.USER_JOINED:
            users[action.id] = action.user;
            UserStore.emitChange();
            break;

        // un utilisateur est parti
        case ActionTypes.USER_LEFT:
            if (users[action.id]) {
                delete users[action.id];
                UserStore.emitChange();
            }
            
            break;
    }
});

module.exports = UserStore;
