'use strict';

var objectAssign = require('object-assign');

var AppDispatcher = require('../dispatcher');
var Store = require('../util/store');
var ActionTypes = require('../../../data/constants').ActionTypes;

var loading = false;
var loadingTimer = null;

/**
 * LoadingStore
 * 
 * Gestion de l'état de chargement dans l'application
 */
var LoadingStore = objectAssign({}, Store.prototype, {
    /**
     * Récupère le message de chargement
     * 
     * @return {string|false} Message de chargement ou "false" si aucun
     */
     getLoading: function () {
         return loading;
     }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        // mettre dans un état de chargement
        case ActionTypes.LOADING_ON:
            if (loadingTimer) {
                clearTimeout(loadingTimer);
            }
            
            // si le serveur répond en moins de 200 ms, inutile
            // d'afficher le chargement
            loadingTimer = setTimeout(function () {
                loading = action.message || 'Chargement…';
                LoadingStore.emitChange();
            }, 200);
            break;

        // retire l'état de chargement
        case ActionTypes.LOADING_OFF:
            if (loadingTimer) {
                clearTimeout(loadingTimer);
                loadingTimer = null;
            }
            
            if (loading) {
                loading = false;
                LoadingStore.emitChange();
            }
            break;
    }
});

module.exports = LoadingStore;
