'use strict';

var objectAssign = require('object-assign');

var AppDispatcher = require('../dispatcher');
var Store = require('../util/store');

var texts = require('../../../data/messages');
var ActionTypes = require('../../../data/constants').ActionTypes;

var messages = {};

/**
 * MessageStore
 * 
 * Gestion des messages d'information de l'application
 */
var MessageStore = objectAssign({}, Store.prototype, {
    /**
     * Vérifie si une erreur de connexion a eu lieu
     * 
     * @return {string|false} Identifiant d'erreur ou "false" si aucune
     */
     getMessages: function () {
         return messages;
     }
});

AppDispatcher.register(function (action) {
    var kind;

    switch (action.actionType) {
        // ajouter un message
        case ActionTypes.ADD_MESSAGE:
            if (Object.keys(texts).indexOf(action.kind) === -1) {
                kind = 'success';
            } else {
                kind = action.kind;
            }

            messages[action.id] = {
                id: action.id,
                kind: kind,
                text: texts[kind][action.type] || texts.default
            };

            MessageStore.emitChange();
            break;
        // supprimer un message
        case ActionTypes.REMOVE_MESSAGE:
            if (messages[action.id] !== undefined) {
                delete messages[action.id];
                MessageStore.emitChange();
            }
            break;
        // supprimer tous les messages
        case ActionTypes.CLEAR_MESSAGES:
            if (Object.keys(messages).length > 0) {
                messages = {};
                MessageStore.emitChange();
            }
            break;
    }
});

module.exports = MessageStore;
