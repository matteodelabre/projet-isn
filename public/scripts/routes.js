'use strict';

var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;

var App = require('./components/app');
var Index = require('./components/index');
var Game = require('./components/game');
var GameReq = require('./components/game-request');
var GamePlay = require('./components/game-play');
var Login = require('./components/login');
var Register = require('./components/register');

module.exports = (
	<Route path="/" handler={App}>
		<DefaultRoute handler={Index} />
        <Route name="game-request" path="/game/request" handler={GameReq} />
        <Route name="game-play" path="/game/:id/play" handler={GamePlay} />
        <Route name="game" path="/game/:id" handler={Game} />
		<Route name="login" path="/user/login" handler={Login} />
		<Route name="register" path="/user/register" handler={Register} />
	</Route>
);
