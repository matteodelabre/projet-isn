/*globals io */

'use strict';

var objectAssign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var constants = require('../../data/constants');
var ActionTypes = constants.ActionTypes;
var MessageTypes = constants.MessageTypes;

var AppDispatcher = require('./dispatcher');
var MessageActions = require('./actions/message-actions');

var router = require('./router');

var address, socket, timer = null;

// connexion au serveur sockets
if (window.location.hostname === 'localhost' ||
        window.location.hostname === '127.0.0.1') {
    address = window.location.origin;
} else {
    address = window.location.hostname + ':8000';
}

socket = io(address);

/**
 * Server
 * 
 * Maintient une interface haut-niveau avec le serveur
 */
var Server = objectAssign({}, EventEmitter.prototype, {
    /**
     * S'authentifie sur un compte existant auprès du serveur
     * 
     * @param {string} name Nom d'utilisateur auquel se connecter
     * @param {string} password Mot de passe de l'utilisateur
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    login: function (name, password, callback) {
        socket.emit('login', name, password);
        socket.once('done', function (data) {
            if (data.kind === 'fail') {
                callback(data.type);
                return;
            }
            
            callback(null, data.user, data.users, data.games);
        });
    },
    
    /**
     * Se déconnecte du compte auquel on est connecté. Cette
     * action ne peut pas échouer, même si l'utilisateur n'est pas connecté
     * 
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    logout: function (callback) {
        socket.emit('logout');
        socket.once('done', function () {
            callback();
        });
    },
    
    /**
     * Crée un nouveau compte sur le serveur
     * 
     * @param {string} name Nom du nouvel utilisateur
     * @param {string} email Adresse mail du compte
     * @param {string} password Mot de passe à associer pour ce compte
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    register: function (name, email, password, callback) {
        socket.emit('register', name, email, password);
        socket.once('done', function (data) {
            if (data.kind === 'fail') {
                callback(data.type);
                return;
            }
            
            callback();
        });
    },
    
    /**
     * Demande à un autre utilisateur de créer une nouvelle partie
     *
     * @param {string} id Identifiant de l'autre utilisateur
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    sendRequest: function (id, callback) {
        socket.emit('game-request', id);
        socket.once('done', function (data) {
            if (data.kind === 'fail') {
                callback(data.type);
                return;
            }
            
            callback(null, data.game);
        });
    },
    
    /**
     * Répond à une demande de nouvelle partie
     *
     * @param {boolean} answer S'il faut accepter ou refuser la demande
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    answerRequest: function (answer, callback) {
        if (timer) {
            clearTimeout(timer);
            timer = null;
        }
        
        socket.emit('game-request-answer', answer);
        
        if (answer) {
            socket.once('done', function (data) {
                if (data.kind === 'fail') {
                    callback(data.type);
                    return;
                }

                callback(null, data.game);
            });
        } else {
            // si l'on a refusé la partie, on sait déjà qu'il n'y
            // en aura pas, donc pas d'attente
            callback();
        }
    },
    
    /**
     * Choisir le thème pour la prochaine série de questions dans
     * une partie
     *
     * @param {string} gameId Identifiant de la partie
     * @param {string} themeId Identifiant du thème choisi
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    select: function (gameId, themeId, callback) {
        socket.emit('game-select', gameId, themeId);
        socket.once('done', function (data) {
            if (data.kind === 'fail') {
                callback(data.type);
                return;
            }
            
            callback(null, data.game);
        });
    },
    
    /**
     * Répond à la question courante dans la partie
     *
     * @param {string} gameId Identifiant de la partie
     * @param {number} i Indice de la réponse
     * @param {function} callback Appelé lorsque le serveur répond
     * @return {null}
     */
    answer: function (gameId, i, callback) {
        socket.emit('game-answer', gameId, i);
        socket.once('done', function (data) {
            if (data.kind === 'fail') {
                callback(data.type);
                return;
            }
            
            callback(null, data.game);
        });
    }
});

// un autre client a rejoint le jeu
socket.on('user-joined', function (id, user) {
    AppDispatcher.dispatch({
        actionType: ActionTypes.USER_JOINED,
        id: id,
        user: user
    });
});

// un des clients connectés est parti
socket.on('user-left', function (id) {
    AppDispatcher.dispatch({
        actionType: ActionTypes.USER_LEFT,
        id: id
    });
});

// demande de jeu
socket.on('game-request', function (id) {
    AppDispatcher.dispatch({
        actionType: ActionTypes.GAME_REQUEST,
        id: id
    });
    
    // si le joueur met trop de temps à répondre, on annule
    timer = setTimeout(function () {
        timer = null;
        
        router.transitionTo('/');
        
        MessageActions.clear();
        MessageActions.add(
            'fail',
            MessageTypes.fail.GAME_REQUEST_TIMEOUT_OPP
        );
    }, 29000);
});

// mise à jour d'une partie
socket.on('game-update', function (game) {
    AppDispatcher.dispatch({
        actionType: ActionTypes.GAME_UPDATE,
        game: game
    });
});

// fin d'une partie : ajout des nouveaux scores
socket.on('game-end', function (id, score, oppId, oppScore) {
    AppDispatcher.dispatch({
        actionType: ActionTypes.USER_ADD_SCORE,
        id: id,
        score: score
    });
    
    AppDispatcher.dispatch({
        actionType: ActionTypes.USER_ADD_SCORE,
        id: oppId,
        score: oppScore
    });
});

module.exports = Server;
