'use strict';

var ActionTypes = require('../../../data/constants').ActionTypes;

var AppDispatcher = require('../dispatcher');
var MessageActions = require('./message-actions');
var LoadingActions = require('./loading-actions');

var server = require('../server');
var router = require('../router');

var GameActions = {
    /**
     * Choisit un nouveau thème sur la partie
     *
     * @param {string} gameId Identifiant de partie
     * @param {string} themeId Identifiant du thème choisi
     * @return {null}
     */
    select: function (gameId, themeId) {
        LoadingActions.setLoading('Sauvegarde du thème');
        
        server.select(gameId, themeId, function (error, game) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (error) {
				MessageActions.add('fail', error);
				return;
			}
            
            AppDispatcher.dispatch({
                actionType: ActionTypes.GAME_UPDATE,
                game: game
            });
        });
    },
    
    /**
     * Répond à la question courante sur la partie
     *
     * @param {string} gameId Identifiant de la partie
     * @param {number} i Indice de la réponse
     * @return {null}
     */
    answer: function (gameId, i) {
        LoadingActions.setLoading('Sauvegarde de la réponse');
        
        server.answer(gameId, i, function (error, game) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (error) {
				MessageActions.add('fail', error);
				return;
			}
            
            AppDispatcher.dispatch({
                actionType: ActionTypes.GAME_UPDATE,
                game: game
            });
        });
    },
    
	/**
	 * Demande la création d'une nouvelle partie contre
     * l'utilisateur donné
	 * 
	 * @param {string} userId Identifiant de l'autre utilisateur
     * @return {null}
	 */
	request: function (userId) {
        LoadingActions.setLoading('En attente de l\'adversaire');
        
        server.sendRequest(userId, function (error, game) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (error) {
				MessageActions.add('fail', error);
				return;
			}
            
            AppDispatcher.dispatch({
                actionType: ActionTypes.GAME_ADD,
                game: game
            });
        });
	},
    
    /**
     * Accepte la demande de nouvelle partie
     *
     * @return {null}
     */
    accept: function () {
        server.answerRequest(true, function (error, game) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (error) {
                router.transitionTo('/');
				MessageActions.add('fail', error);
				return;
			}
            
            AppDispatcher.dispatch({
                actionType: ActionTypes.GAME_ADD,
                game: game
            });
        });
    },
    
    /**
     * Refuse la demande de nouvelle partie
     *
     * @return {null}
     */
    decline: function () {
        server.answerRequest(false, function () {
            AppDispatcher.dispatch({
                actionType: ActionTypes.GAME_REQUEST_DECLINE
            });
        });
    }
};

module.exports = GameActions;
