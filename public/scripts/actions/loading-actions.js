'use strict';

var ActionTypes = require('../../../data/constants').ActionTypes;
var AppDispatcher = require('../dispatcher');

var LoadingActions = {
	/**
	 * Modifie l'état de chargement de l'application
	 * 
	 * @param {string|false} message Message de chargement, ou "false"
	 */
	setLoading: function (message) {
        if (message) {
            AppDispatcher.dispatch({
                actionType: ActionTypes.LOADING_ON,
                message: message
            });
        } else {
            AppDispatcher.dispatch({
                actionType: ActionTypes.LOADING_OFF
            });
        }
	}
};

module.exports = LoadingActions;
