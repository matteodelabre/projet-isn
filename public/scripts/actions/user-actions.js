'use strict';

var constants = require('../../../data/constants');
var ActionTypes = constants.ActionTypes;
var MessageTypes = constants.MessageTypes;

var AppDispatcher = require('../dispatcher');
var MessageActions = require('./message-actions');
var LoadingActions = require('./loading-actions');

var server = require('../server');
var router = require('../router');

var UserActions = {
	/**
	 * Action de connexion au serveur
	 * 
	 * @param {string} name Nom de l'utilisateur auquel se connecter
	 * @param {string} password Mot de passe de cet utilisateur
	 * @return {null}
	 */
	login: function (name, password) {
        LoadingActions.setLoading('Connexion en cours…');
        
		server.login(name, password, function (error, user, users, games) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (error) {
				MessageActions.add('fail', error);
				return;
			}
			
			MessageActions.add('success', MessageTypes.success.USER_LOGIN);
			router.transitionTo('/');
            
			AppDispatcher.dispatch({
				actionType: ActionTypes.USER_LOGIN,
				user: user,
				users: users,
                games: games
			});
		});
	},
	
	/**
	 * Action de déconnexion du serveur
	 * 
	 * @return {null}
	 */
	logout: function () {
        LoadingActions.setLoading('Déconnexion…');
        
		server.logout(function () {
            LoadingActions.setLoading(false);
			MessageActions.clear();
            
			MessageActions.add('success', MessageTypes.success.USER_LOGOUT);
			router.transitionTo('/');
            
			AppDispatcher.dispatch({
				actionType: ActionTypes.USER_LOGOUT
			});
		});
	},
	
	/**
	 * Action d'inscription sur le serveur
	 * 
	 * @param {string} name Nom de l'utilisateur auquel se connecter
	 * @param {string} password Mot de passe de cet utilisateur
	 * @return {null}
	 */
	register: function (name, email, password) {
        LoadingActions.setLoading('Inscription en cours…');
        
		server.register(name, email, password, function (err) {
            LoadingActions.setLoading(false);
			MessageActions.clear();
			
			if (err) {
				MessageActions.add('fail', err);
				return;
			}
			
			MessageActions.add('success', MessageTypes.success.USER_REGISTER);
			router.transitionTo('/');
            
			AppDispatcher.dispatch({
				actionType: ActionTypes.USER_REGISTER
			});
		});
	},
    
    /**
     * Ajoute un score à l'utilisateur
     *
     * @param {string} id Identifiant de l'utilisateur
     * @param {number} score Score à ajouter
     * @return {null}
     */
    addScore: function (id, score) {
        AppDispatcher.dispatch({
            actionType: ActionTypes.USER_ADD_SCORE,
            id: id,
            score: score
        });
    }
};

module.exports = UserActions;
