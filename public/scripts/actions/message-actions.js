'use strict';

var uid = require('uid');

var ActionTypes = require('../../../data/constants').ActionTypes;
var AppDispatcher = require('../dispatcher');

var MessageActions = {
	/**
	 * Ajoute un ou plusieurs messages
	 * 
	 * @param {string} kind Catégorie des messages ('fail' ou 'success')
	 * @param {Array<string>|string} type Message ou liste de messages
	 * @return {Array<string>|string} id Identifiant du(des) messages créés
	 */
	add: function (kind, type) {
		var id;
		
		if (Array.isArray(type)) {
			return type.map(function (subtype) {
				this.add(kind, subtype);
			}, this);
		} else {
			id = uid(10);
			AppDispatcher.dispatch({
				actionType: ActionTypes.ADD_MESSAGE,
				id: id,
				kind: kind,
				type: type
			});
			
			return id;
		}
	},
	
	/**
	 * Supprime un message
	 * 
	 * @param {string} id Identiant du message à supprimer
	 * @return {null}
	 */
	remove: function (id) {
		AppDispatcher.dispatch({
			actionType: ActionTypes.REMOVE_MESSAGE,
			id: id
		});
	},
	
	/**
	 * Efface tous les messages
	 * 
	 * @return {null}
	 */
	clear: function () {
		AppDispatcher.dispatch({
			actionType: ActionTypes.CLEAR_MESSAGES
		});
	}
};

module.exports = MessageActions;
