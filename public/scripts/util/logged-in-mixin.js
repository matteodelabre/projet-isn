'use strict';

var UserStore = require('../stores/user-store');

/**
 * Mixin qui s'assure que l'on ne puisse pas accéder au composant
 * si l'on n'est pas connecté
 * 
 * @param {Array<Object>} stores Liste des entrepôts requis
 * @param {function} getState Pour calculer l'état actuel du composant
 * @return {Object} Mixin résultante
 */
var LoggedInMixin = {
    statics: {
        willTransitionTo: function (transition) {
            if (!UserStore.getUser()) {
                transition.abort();
            }
        }
    }
};

module.exports = LoggedInMixin;
