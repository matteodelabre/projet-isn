'use strict';

/**
 * Mixin pour les composants qui dépendent sur des entrepôts
 * 
 * @param {Array<Object>} stores Liste des entrepôts requis
 * @param {function} getState Pour calculer l'état actuel du composant
 * @return {Object} Mixin résultante
 */
var getStoreListenerMixin = function (stores, getState) {
	if (!Array.isArray(stores)) {
		stores = [stores];
	}
	
	return {
		handleStoreChange: function () {
			this.setState(getState());
		},
		
		componentDidMount: function () {
			stores.forEach(function (store) {
				store.addChangeListener(this.handleStoreChange);
			}, this);
		},
		
		componentWillUnmount: function () {
			stores.forEach(function (store) {
				store.removeChangeListener(this.handleStoreChange);
			}, this);
		}
	};
};

module.exports = getStoreListenerMixin;
