'use strict';

var EventEmitter = require('events').EventEmitter;
var objectAssign = require('object-assign');

/**
 * Store
 * 
 * Objet générique pour les entrepôts de données
 * fournissant les méthodes communes
 * 
 * @constructor
 */
function Store() {}

Store.prototype = objectAssign({}, EventEmitter.prototype, {
	/**
	 * Déclenche un événement pour notifier d'un changement
	 * dans l'entrepôt
	 * 
	 * @return {null}
	 */
	emitChange: function () {
		this.emit('change');
	},
	
	/**
	 * Ajoute un écouteur pour l'événement de changement
	 * 
	 * @param {function} callback Appelée à chaque changement
	 * @return {null}
	 */
	addChangeListener: function (callback) {
		this.addListener('change', callback);
	},
	
	/**
	 * Retire un écouteur pour l'événement de changement
	 * 
	 * @param {function} callback Écouteur déjà enregistré
	 * @return {null}
	 */
	removeChangeListener: function (callback) {
		this.removeListener('change', callback);
	}
});

module.exports = Store;
