'use strict';

/**
 * @var {Object} Styles communs des paragraphes
 */
exports.paragraphs = {
	margin: '0.75em 0'
};

/**
 * @var {string} Légère ombre
 */
exports.shadow = '0 2px 2px rgba(0, 0, 0, 0.1)';

/**
 * @var {Object} Styles communs aux boutons et entrées
 */
exports.formInputs = {
	appearance: 'none',
	display: 'block',
	
	width: '100%',
	height: 40,
	verticalAlign: 'top',
	
	fontFamily: 'inherit',
	fontSize: '1em',
	
	padding: '6px 12px',
	border: 0
};

/**
 * @var {Object} Styles communs aux éléments d'information
 */
exports.info = {
    base: {
        overflow: 'hidden',
        marginBottom: '1em'
    },
    
    aside: {
        display: 'inline-block',
        verticalAlign: 'middle'
    }
};

/**
 * @var {Object} Couleurs communes
 */
exports.colors = {
    red: '#C62828',
    green: '#2E7D32'
};
