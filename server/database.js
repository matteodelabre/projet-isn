'use strict';

var mongoose = require('mongoose');
var logger = require('./logger');

/**
 * Se connecte à la base de données et récupère les modèles
 * 
 * @param {function} callback Appelé avec les modèles ou une erreur
 * @return {null}
 */
exports.connect = function (callback) {
    var connection, url;
    
    // récupération des identifiants
    url = 'mongodb://localhost/isn';
    
    if (process.env.OPENSHIFT_APP_NAME) {
        url = process.env.OPENSHIFT_MONGODB_DB_URL +
            process.env.OPENSHIFT_APP_NAME;
    } else {
        url = 'mongodb://localhost/isn';
    }
    
    // ouverture de la connexion à la base de données
    connection = mongoose.createConnection();
    connection.on('error', function (err) {
        callback(err);
    });
    
    connection.open(url, function () {
        callback(null, {
            User: require('./models/user')(connection),
            Game: require('./models/game')(connection)
        });
    });
};
