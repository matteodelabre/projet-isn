'use strict';

/**
 * Point d'entrée du serveur
 * 
 * Établit une connexion à la base de données puis démarre
 * le serveur web et le serveur sockets
 * 
 * @see database.js
 * @see server.js
 * @see server-socket.js
 * @see server-web.js
 */

var logger = require('./logger');
var database = require('./database');
var server = require('./server');

logger.info('Connexion à la base de données...');
database.connect(function (err, models) {
    if (err) {
        logger.error('Échec de connexion :', err.stack || err);
        return;
    }
    
    logger.info('Démarrage du serveur...');
    server.createServer(models, function (err, ip, port) {
        if (err) {
            logger.error('Échec du démarrage serveur :', err.stack || err);
            return;
        }
        
        logger.info('En attente de connexions sur %s:%s', ip, port);
    });
});
