'use strict';

var objectAssign = require('object-assign');

var random = require('./random');
var logger = require('./logger');
var Socket = require('./sockets/socket');
var Namespace = require('./sockets/namespace');

var MessageTypes = require('../data/constants').MessageTypes;
var themes = require('../data/themes');
var answers = require('../data/answers');

/**
 * Transforme les messages d'erreurs relatifs à la base de données
 * en une liste de types de messages d'erreurs conformes
 * à la représentation de l'application
 * 
 * @param {Object|null} err Liste d'erreurs
 * @return {Array<string>|false} Liste d'erreurs transformées
 */
function transformErrors(err) {
    var messages, errors, key;
    
    if (typeof err !== 'object' || err === null) {
        return false;
    }
    
    // conversion des erreurs de validation en une liste
    // de types d'erreurs
    if (err.name === 'ValidationError') {
        errors = err.errors;
        messages = [];
        
        for (key in errors) {
            if (errors.hasOwnProperty(key)) {
                messages.push(errors[key].message);
            }
        }
        
        return messages;
    }
    
    return [MessageTypes.fail.SERVER];
}

/**
 * Gestion des connexions par Socket
 *
 * @param {Object} models Modèle de la base de données
 * @param {Object} namespace Espace de noms à gérer
 * @return {null}
 */
module.exports = function (models, namespace) {
    var User = models.User,
        Game = models.Game;
    
    namespace = objectAssign(namespace, Namespace);
    
    // pour chaque utilisateur qui se connecte
    namespace.on('connection', function (socket) {
        socket.user = false;
        socket = objectAssign(socket, Socket);
        namespace.printUsers();
        
        /**
         * Connexion à un compte existant
         *
         * @param {string} name Nom d'utilisateur du compte
         * @param {string} password Mot de passe associé
         */
        socket.on('login', function (name, password) {
            User.findUser(name, password, function (err, user) {
                if (err) {
                    socket.emitFail(err);
                    return;
                }
                
                if (namespace.findUser(user._id) !== false) {
                    socket.emitFail(MessageTypes.fail.USER_LOGIN_ALREADY);
                    return;
                }
                
                socket.user = user;
                
                // récupère une version sûre pour la diffusion
                // de l'utilisateur
                user.safe(function (safeErr, safe) {
                    if (safeErr) {
                        socket.emitFail(MessageTypes.fail.SERVER);
                        return;
                    }
                    
                    safe.id = socket.id;

                    // rejoint la liste des connectés, et notifie
                    // les connectés qu'un utilisateur s'est connecté
                    socket.join('logged');
                    socket.to('logged').emit('user-joined', socket.id, safe);

                    // met à jour l'affichage du nombre de connectés
                    namespace.printUsers();
                    
                    // récupère toutes les parties de l'utilisateur
                    Game.findGames(user.id, function (gameErr, games) {
                        if (gameErr) {
                            socket.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }
                        
                        // récupère tous les autres utilisateurs
                        socket.getUsers(function (usersErr, users) {
                            if (usersErr) {
                                socket.emitFail(MessageTypes.fail.SERVER);
                                return;
                            }

                            socket.emitSuccess({
                                user: safe,
                                users: users,
                                games: games
                            });
                        });
                    });
                });
            });
        });
        
        /**
         * Déconnexion du compte, si l'utilisateur est connecté
         */
        socket.on('logout', function () {
            if (socket.isLoggedIn()) {
                socket.user = false;
                socket.leave('logged');
                
                socket.to('logged').emit('user-left', socket.id);
                namespace.printUsers();
            }
            
            socket.emitSuccess();
        });
        
        /**
         * Lorsque le socket est déconnecté de manière inattendue
         */
        function emergencyLogout() {            
            // ici, le socket a déjà été déconnecté de la salle
            // "logged", aucune manière fiable de savoir s'il était
            // connecté à un utilisateur ou non, donc dans tous les
            // cas on le traite comme s'il l'était
            socket.to('logged').emit('user-left', socket.id);
            namespace.printUsers();
        }
        
        socket.on('disconnect', emergencyLogout);
        socket.on('error', function (err) {
            logger.error('Socket error:', err.stack || err);
            emergencyLogout();
        });
        
        /**
         * Inscription de l'utilisateur
         *
         * @param {string} name Nom d'utilisateur à utiliser
         * @param {string} email Adresse mail du compte
         * @param {string} password Mot de passe à associer au nouveau compte
         */
        socket.on('register', function (name, email, password) {
            var user = new User({
                name: name,
                email: email,
                password: password
            });
            
            user.trySave(function (err) {
                err = transformErrors(err);
                
                if (err) {
                    socket.emitFail(err);
                    return;
                }
                
                socket.emitSuccess();
            });
        });

        /**
         * Lorsque l'utilisateur souhaite démarrer une partie
         *
         * @param {int} id L'ID de l'autre utilisateur
         */
        socket.on('game-request', function (id) {
            var other, answerHandler, timer;
            
            if (namespace.connected[id]) {
                other = objectAssign(namespace.connected[id], Socket);
            } else {
                other = false;
            }
            
            if (!socket.isLoggedIn()) {
                socket.emitFail(MessageTypes.fail.GAME_REQUEST_NOT_LOGGED);
                return;
            }

            if (!other || !other.isLoggedIn()) {
                socket.emitFail(MessageTypes.fail.GAME_REQUEST_UNKNOWN_OPP);
                return;
            }
            
            other.emit('game-request', socket.id);
            answerHandler = function (accept) {
                var game;
                
                // retire le timeout
                clearTimeout(timer);
                
                if (!accept) {
                    socket.emitFail(MessageTypes.fail.GAME_REQUEST_DECLINED);
                    return;
                }
                
                // création d'un nouvel objet de jeu représentant
                // la partie entre les deux joueurs
                game = new Game({
                    user1: socket.user._id,
                    user2: other.user._id
                });
                
                game.save(function (saveErr, saved) {
                    if (saveErr) {
                        socket.emitFail(MessageTypes.fail.SERVER);
                        other.emitFail(MessageTypes.fail.SERVER);
                        return;
                    }
                    
                    // envoi de la représentation de la partie
                    // au demandeur
                    saved.safe(socket.user.id, function (safeErr, safe) {
                        if (safeErr) {
                            socket.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }
                        
                        socket.emitSuccess({game: safe});
                    });
                    
                    // envoi de la représentation de la partie
                    // au receveur
                    saved.safe(other.user.id, function (safeErr, safe) {
                        if (safeErr) {
                            other.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }
                        
                        other.emitSuccess({game: safe});
                    });
                });
            };
            
            // après 30 secondes, émettre un timeout
            timer = setTimeout(function () {
                other.removeListener('game-request-answer', answerHandler);
                socket.emitFail(MessageTypes.fail.GAME_REQUEST_TIMEOUT);
            }, 30000);
            
            other.once('game-request-answer', answerHandler);
        });
        
        /**
         * Choix du thème pour la prochaine série de question,
         * génération des questions et mise à jour de l'objet de jeu
         *
         * @param {string} gameId Identifiant de la partie
         * @param {string} themeId Identifiant du thème choisi
         */
        socket.on('game-select', function (gameId, themeId) {
            Game.findGame(gameId, function (err, game) {
                var theme, gameThemes;
                
                if (err) {
                    socket.emitFail(err);
                    return;
                }
                
                if (game.getUserNumber(socket.user.id) !== game.playing) {
                    socket.emitFail(MessageTypes.fail.GAME_PLAY_NOT_PLAYING);
                    return;
                }
                
                theme = themes[themeId];
                gameThemes = game.themes.map(function (subTheme) {
                    return subTheme.themeId;
                });
                
                if (gameThemes.indexOf(themeId) > -1) {
                    socket.emitFail(MessageTypes.fail.GAME_SELECT_THEME_REP);
                    return;
                }
                
                if (!theme) {
                    socket.emitFail(MessageTypes.fail.GAME_SELECT_THEME_INV);
                    return;
                }
                
                // ajout d'un nouveau thème avec trois questions
                // au hasard
                game.themes.push({
                    themeId: themeId,
                    questions: random.pick(theme.questions, 3).map(
                        function (obj) {
                            return {questionId: obj.id};
                        }
                    )
                });
                
                game.safe(socket.user.id, function (safeErr, safe) {
                    if (safeErr) {
                        socket.emitFail(MessageTypes.fail.SERVER);
                        return;
                    }
                    
                    game.save(function (saveErr) {
                        if (saveErr) {
                            socket.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }

                        socket.emitSuccess({game: safe});
                    });
                });
            });
        });
        
        /**
         * Réponse à la question courante de la partie
         *
         * @param {string} gameId Identifiant de la partie
         * @param {number} i Indice de la réponse
         */
        socket.on('game-answer', function (gameId, index) {
            Game.findGame(gameId, function (err, game) {
                var theme, questions, question,
                    found = false, length, i,
                    number = game.getUserNumber(socket.user.id),
                    opponent = (number === 1) ? 2 : 1;
                
                if (err) {
                    socket.emitFail(err);
                    return;
                }
                
                if (number !== game.playing) {
                    socket.emitFail(MessageTypes.fail.GAME_PLAY_NOT_PLAYING);
                    return;
                }
                
                theme = game.themes[game.themes.length - 1];
                questions = theme.questions;
                length = questions.length;
                
                // on boucle jusqu'à tomber sur la première
                // question sans réponse
                for (i = 0; i < length; i += 1) {
                    question = questions[i];
                    
                    if (!question['user' + number]) {
                        found = true;
                        
                        question['user' + number] = true;
                        question['user' + number + 'Correct'] =
                            (answers[theme.themeId][question.questionId] ===
                                index);
                        
                        break;
                    }
                }
                
                if (!found) {
                    socket.emitFail(MessageTypes.fail.SERVER);
                    return;
                }
                
                // si l'on a terminé le thème, marquer le thème
                // comme terminé et passer la main à l'autre utilisateur
                if (i === 2) {
                    game.playing = opponent;
                    theme['user' + number] = true;
                }
                
                // si tout le monde à répondu au dernier thème,
                // marquer la partie comme terminée
                if (theme.user1 && theme.user2 &&
                        game.themes.length === 3) {
                    game.finished = true;

                    // accumulation du score dès que possible
                    User.addScore(game.user1, game.getScore(1));
                    User.addScore(game.user2, game.getScore(2));
                }
                
                game.save(function (saveErr) {
                    var other = namespace.findUser(game['user' + opponent]);

                    if (saveErr) {
                        socket.emitFail(MessageTypes.fail.SERVER);
                        return;
                    }
                    
                    // mise à jour des infos des deux côtés
                    game.safe(socket.user.id, function (safeErr, safe) {
                        if (safeErr) {
                            socket.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }
                        
                        socket.emitSuccess({game: safe});
                        
                        if (game.finished) {
                            socket.emit(
                                'game-end',
                                socket.id, safe.score,
                                other.id, safe.oppScore
                            );
                        }
                    });
                    
                    game.safe(other.user.id, function (oppErr, opp) {
                        if (oppErr) {
                            socket.emitFail(MessageTypes.fail.SERVER);
                            return;
                        }

                        other.emit('game-update', opp);
                        
                        if (game.finished) {
                            other.emit(
                                'game-end',
                                other.id, opp.score,
                                socket.id, opp.oppScore
                            );
                        }
                    });
                });
            });
        });
        
        // fin de l'écoute d'événements
    });
};
