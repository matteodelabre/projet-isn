'use strict';

// sauvegarde les paramètres du journaliseur
module.exports = require('tracer').console({
    format: "{{timestamp}} <{{title}}> {{message}}",
    dateformat: "HH:MM:ss"
});
