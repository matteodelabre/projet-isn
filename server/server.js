'use strict';

var Server = require('http').Server;
var express = require('express');
var socketIO = require('socket.io');

var webServer = require('./server-web');
var socketServer = require('./server-socket');

/**
 * Crée un serveur Web et Socket
 * 
 * @param {Object} models Modèles de la base de données
 * @param {function} callback Appelé avec une erreur ou l'ip/port utilisé
 * @eturn {null}
 */
exports.createServer = function (models, callback) {
	var app, server, io, port, ip;
	
    ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
    port = process.env.OPENSHIFT_NODEJS_PORT || 80;
	
	app = express();
	server = new Server(app);
	io = socketIO(server);
	
    server.listen(port, ip, function (err) {
        if (err) {
            callback(err);
            return;
        }
        
        // délègue la gestion du web et des sockets aux sous-serveurs
        webServer(app);
        socketServer(models, io.sockets);
        
        callback(null, ip, port);
    });
};
