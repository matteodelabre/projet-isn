'use strict';

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var async = require('async');

var MessageTypes = require('../../data/constants').MessageTypes;

module.exports = function (db) {
    var Game, gameSchema;
    
    /**
     * Schéma des parties
     *
     * @constructor
     * @prop {Number} playing Numéro de l'utilisateur ayant la main
     * @prop {User} user1 Utilisateur initiateur de la partie
     * @prop {User} user2 Adversaire de la partie
     * @prop {Date} startTime Date du début de la partie
     * @prop {Array<object>} themes Sauvegarde des questions déjà traitées
     */
    gameSchema = new mongoose.Schema({
        playing: {
            type: Number,
            default: 2
        },
        
        user1: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        
        user2: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        
        finished: {
            type: Boolean,
            default: false
        },
        
        startTime: Date,
        
        themes: [{
            themeId: {
                type: String,
                required: true
            },
            
            user1: {
                type: Boolean,
                default: false
            },
            
            user2: {
                type: Boolean,
                default: false
            },
            
            questions: [{
                questionId: {
                    type: Number,
                    required: true
                },
                
                user1: {
                    type: Boolean,
                    default: false
                },
                
                user2: {
                    type: Boolean,
                    default: false
                },
                
                user1Correct: {
                    type: Boolean,
                    default: false
                },
                
                user2Correct: {
                    type: Boolean,
                    default: false
                }
            }]
        }]
    });
    
    // assigne la date actuelle aux nouvelles parties
    gameSchema.pre('save', function (next) {
        if (!this.startTime) {
            this.startTime = new Date();
        }
        
        next();
    });
    
    /**
     * Détermine le numéro de l'utilisateur dans la partie
     *
     * @param {string} id Identifiant unique de l'utilisateur
     * @return {number} 1 ou 2
     */
    gameSchema.method('getUserNumber', function (id) {
        if (!id) {
            return false;
        }
        
        if (id.toString() === this.user1.toString()) {
            return 1;
        }
        
        if (id.toString() === this.user2.toString()) {
            return 2;
        }
        
        return false;
    });
    
    /**
     * Calcule le score de l'utilisateur donné
     *
     * @param {number} num Numéro de l'utilisateur dans la partie
     * @return {number} Score correspondant
     */
    gameSchema.method('getScore', function (num) {
        var themesLength, questionsLength,
            themes, questions,
            theme, question,
            score = 0, i, j;
        
        themes = this.themes;
        themesLength = themes.length;
        
        for (i = 0; i < themesLength; i += 1) {
            theme = themes[i];
            questions = theme.questions;
            questionsLength = questions.length;
            
            for (j = 0; j < questionsLength; j += 1) {
                question = questions[j];
                
                if (question['user' + num + 'Correct']) {
                    score += 1;
                }
            }
        }
        
        return score;
    });
    
    /**
     * Détermine une représentation sûre de la partie,
     * c'est-à-dire sans les propriétés sensibles
     *
     * @param {string} id Identifiant unique de l'utilisateur actuel
     * @param {function} callback Appelée avec la représentation sûre
     * @return {null}
     */
    gameSchema.method('safe', function (id, callback) {
        var User = db.models.User, oppId,
            oppType, ourType, themes = [],
            gameId = this._id, finished = this.finished;
        
        if (id.toString() !== this.user1.toString()) {
            oppId = this.user1;
            oppType = 1;
            ourType = 2;
        } else {
            oppId = this.user2;
            oppType = 2;
            ourType = 1;
        }
        
        // résoud la liste de thèmes
        themes = this.themes.map(function (theme) {
            return {
                themeId: theme.themeId,
                played: theme['user' + ourType],
                
                questions: theme.questions.map(function (question) {
                    return {
                        questionId: question.questionId,
                        played: question['user' + ourType],
                        oppPlayed: question['user' + oppType],
                        
                        correct: question['user' + ourType + 'Correct'],
                        oppCorrect: question['user' + oppType + 'Correct']
                    };
                })
            };
        }, this);
        
        // résoud l'utilisateur courant
        User.findOne({
            _id: oppId
        }, function (err, opponent) {
            if (err) {
                callback(err);
                return;
            }
            
            if (!opponent) {
                callback(new Error('Unknown opponent'));
                return;
            }
            
            opponent.safe(function (safeErr, safe) {
                if (safeErr) {
                    callback(safeErr);
                    return;
                }
                
                callback(null, {
                    id: gameId,
                    
                    finished: finished,
                    playing: !finished && this.playing !== oppType,
                    opponent: safe,
                    
                    score: this.getScore(ourType),
                    oppScore: this.getScore(oppType),
                    
                    startTime: this.startTime,
                    themes: themes
                });
            }.bind(this));
        }.bind(this));
    });
    
    /**
     * Récupère les parties d'un utilisateur
     *
     * @param {string} userId Identifiant unique de l'utilisateur
     * @param {function} callback Appelé après avoir trouvé les parties
     */
    gameSchema.static('findGames', function (userId, callback) {
        Game.find({
            finished: false,
            
            $or: [
                {user1: userId},
                {user2: userId}
            ]
        }, function (findErr, games) {
            var safeGames = {};
            
            if (findErr) {
                callback(findErr);
                return;
            }
            
            async.forEach(games, function (game, mapCallback) {
                game.safe(userId, function (err, safe) {
                    if (err) {
                        mapCallback(err);
                        return;
                    }
                    
                    safeGames[safe.id] = safe;
                    mapCallback();
                });
            }, function (safeErr) {
                callback(safeErr, safeGames);
            });
        });
    });
    
    /**
     * Retrouve un objet de jeu en fonction de son identifiant
     *
     * @param {string} gameId Identifiant unique de la partie
     * @param {function} callback Appelée lorsque l'on a retrouvé la partie
     * @return {false}
     */
    gameSchema.static('findGame', function (gameId, callback) {
        Game.findOne({
            _id: new ObjectId(gameId)
        }, function (findErr, game) {
            if (findErr) {
                callback(MessageTypes.fail.SERVER);
                return;
            }

            if (!game) {
                callback(MessageTypes.fail.GAME_PLAY_UNKNOWN);
                return;
            }
            
            if (game.finished) {
                callback(MessageTypes.fail.GAME_PLAY_FINISHED);
                return;
            }
            
            callback(null, game);
        });
    });
    
    Game = db.model('Game', gameSchema);
    return Game;
};
