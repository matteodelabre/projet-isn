'use strict';

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var validator = require('validator');
var crypto = require('crypto');
var saltLength = 64;

var MessageTypes = require('../../data/constants').MessageTypes;

/**
 * Génère une clef de salage pour les mots de passe
 *
 * @param {number} size Longueur de la clef
 * @return {string} Valeur de la clef
 */
function generateSalt(size) {
    return crypto.randomBytes(Math.ceil(size / 2))
        .toString('hex')
        .slice(0, size);
}

/**
 * Hache un mot de passe pour la sauvegarde dans la base de données
 *
 * @param {string} pwd Mot de passe à hacher
 * @param {function} callback Appelé avec le résultat, ou une erreur
 * @return {null}
 */
function hashPassword(pwd, callback) {
    var salt = generateSalt(saltLength);
    
    crypto.pbkdf2(
        pwd, salt, 4096, saltLength,
        function (hashErr, key) {
            if (hashErr) {
                callback(hashErr);
                return;
            }
            
            callback(null, salt + key.toString('hex'));
        }
    );
}

/**
 * Vérifie si un mot de passe correspond au mot de passe haché donné
 *
 * @param {string} pwd Mot de passe à vérifier
 * @param {string} hash Mot de passe haché pour référence
 * @param {function} callback Appelé avec le résultat, ou une erreur
 * @return {null}
 */
function checkPassword(pwd, hash, callback) {
    var salt = hash.substr(0, saltLength),
        compare = hash.substr(saltLength);
    
    crypto.pbkdf2(
        pwd, salt, 4096, saltLength,
        function (err, key) {
            if (err) {
                callback(err);
                return;
            }
            
            callback(null, key.toString('hex') === compare);
        }
    );
}

module.exports = function (db) {
    var User, userSchema;
    
    /**
     * Schéma utilisateur
     *
     * @constructor
     * @prop {string} name Nom de l'utilisateur
     * @prop {string} email Email à associer au compte
     * @prop {string} password Mot de passe
     * @prop {number} score Score cumulé du compte
     */
    userSchema = new mongoose.Schema({
        name: {
            type: String,
            unique: MessageTypes.fail.USER_REGISTER_NAME_NUQ,
            required: MessageTypes.fail.USER_REGISTER_NAME_INVALID,
            match: [
                /[a-zA-Z0-9@_:\/()-]{4,20}/,
                MessageTypes.fail.USER_REGISTER_NAME_INVALID
            ]
        },

        email: {
            type: String,
            unique: MessageTypes.fail.USER_REGISTER_EMAIL_NUQ,
            required: MessageTypes.fail.USER_REGISTER_EMAIL_INVALID,
            validate: [
                validator.isEmail,
                MessageTypes.fail.USER_REGISTER_EMAIL_INVALID
            ]
        },

        password: {
            type: String,
            required: MessageTypes.fail.USER_REGISTER_PWD_INVALID,
            match: [
                /.{8,64}/,
                MessageTypes.fail.USER_REGISTER_PWD_INVALID
            ]
        },
        
        creationTime: Date,

        score: {
            type: Number,
            default: 0,
            min: 0
        }
    });
    
    userSchema.plugin(require('mongoose-beautiful-unique-validation'));

    // assigne la date actuelle aux nouveaux utilisateurs
    // et hache le mot de passe à chaque modification
    userSchema.pre('save', function (next) {
        if (!this.creationTime) {
            this.creationTime = new Date();
        }
        
        if (!this.isModified('password')) {
            next();
            return;
        }

        hashPassword(this.password, function (err, result) {
            if (err) {
                next(err);
                return;
            }

            this.password = result;
            next();
        }.bind(this));
    });
    
    /**
     * Propriété calculée pour récupérer le Gravatar en fonction
     * de l'adresse mail de l'utilisateur
     *
     * @prop {string} avatar Lien vers le Gravatar de l'utilisateur
     */
    userSchema.virtual('avatar').get(function () {
        var hash = crypto.createHash('md5').update(this.email).digest('hex');
        
        return 'https://secure.gravatar.com/avatar/' + hash + '?d=mm';
    });
    
    /**
     * Détermine une représentation sûre de l'utilisateur,
     * c'est-à-dire sans les propriétés sensibles
     *
     * @param {function} callback Appelée avec la représentation sûre
     * @return {null}
     */
     userSchema.method('safe', function (callback) {
         callback(null, {
             name: this.name,
             avatar: this.avatar,
             creationTime: this.creationTime,
             score: this.score
         });
     });

    /**
     * Récupère un utilisateur en fonction de son nom et de son mot
     * de passe
     *
     * @param {string} name Nom d'utilisateur
     * @param {string} pwd Mot de passe de l'utilisateur
     * @param {function} callback Appelé après avoir trouvé l'utilisateur
     */
    userSchema.static('findUser', function (name, pwd, callback) {
        User.findOne({
            name: name
        }, function (findErr, user) {
            if (findErr) {
                callback(MessageTypes.fail.SERVER);
                return;
            }

            if (!user) {
                callback(MessageTypes.fail.USER_LOGIN_NO_MATCH);
                return;
            }

            checkPassword(pwd, user.password, function (checkErr, match) {
                if (checkErr) {
                    callback(MessageTypes.fail.SERVER);
                    return;
                }

                if (!match) {
                    callback(MessageTypes.fail.USER_LOGIN_NO_MATCH);
                    return;
                }

                callback(null, user);
            });
        });
    });
    
    /**
     * Ajoute à l'utilisateur donné le score donné
     *
     * @param {string} id Identifiant unique de l'utilisateur
     * @param {number} score Score à ajouter
     * @param {function} callback Appelé après avoir ajouté le score
     */
    userSchema.static('addScore', function (id, score, callback) {
        User.findOne({
            _id: new ObjectId(id)
        }, function (err, user) {
            if (err) {
                callback(err);
                return;
            }
            
            if (!user) {
                callback(new Error('User not found'));
                return;
            }
            
            user.score += score;
            user.save(callback);
        });
    });
    
    User = db.model('User', userSchema);
    return User;
};
