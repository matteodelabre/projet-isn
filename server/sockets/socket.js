'use strict';

var async = require('async');
var objectAssign = require('object-assign');

/**
 * Socket
 * 
 * Méthodes et propriétés spécifiques à cette application pour toutes
 * les instances de sockets qui se connectent
 */
var Socket = {
    /**
     * Signale que l'action s'est terminée avec succès
     *
     * @param {Object} [data={}] Données supplémentaires à fournir
     * @return {null}
     */
    emitSuccess: function (data) {
        if (typeof data !== 'object' || data === null) {
            data = {};
        }
        
        this.emit('done', objectAssign({
            kind: 'success'
        }, data));
    },
    
    /**
     * Signale que l'action est terminée et a échoué
     * 
     * @param {Array<string>|string} type Erreur(s) produite(s)
     */
    emitFail: function (type) {
        this.emit('done', {
            kind: 'fail',
            type: type
        });
    },
    
    /**
     * Vérifie si l'utilisateur est connecté ou non
     * 
     * @return {boolean} Si l'utilisateur est connecté, true
     */
     isLoggedIn: function () {
         return this.rooms.indexOf('logged') > -1;
     },
    
    /**
     * Gènère une liste des autres utilisateurs connectés
     *
     * @param {function} callback Appelée avec la liste générée
     * @return {null}
     */
    getUsers: function (callback) {
        var sockets = this.nsp.connected, ids,
            users = {};
        
        ids = Object.keys(sockets).filter(function (id) {
            var user = sockets[id].user;
            
            return id !== this.id & user !== false;
        }, this);
        
        async.forEach(ids, function (id, eachCallback) {
            sockets[id].user.safe(function (err, safe) {
                if (err) {
                    eachCallback(err);
                    return;
                }
                
                safe.id = id;
                users[id] = safe;
                
                eachCallback();
            });
        }, function (err) {
            callback(err, users);
        });
    }
};

module.exports = Socket;
