'use strict';

var logger = require('../logger');

/**
 * Namespace
 * 
 * Méthodes et propriétés spécifiques à cette application pour les
 * espaces de sockets
 */
var Namespace = {
    /**
     * Récupère le socket connecté à l'utilisateur donné,
     * si un socket y est connecté, "false" sinon
     *
     * @param {string} id Identifiant unique de l'utilisateur
     * @return {Object|false} Socket connecté, ou "false"
     */
    findUser: function (id) {
        var logged = this.connected || {}, key;
        
        for (key in logged) {
            if (logged.hasOwnProperty(key) && logged[key].user &&
                    logged[key].user._id.toString() === id.toString()) {
                return logged[key];
            }
        }
        
        return false;
    },
    
    /**
     * Affiche le nombre d'utilisateurs en ligne et le nombre
     * d'utilisateurs connectés à leur compte actuellement
     * 
     * @return {null}
     */
    printUsers: function () {
        var logged = this.adapter.rooms.logged || {};
        
        logger.debug(
            '%d utilisateurs en ligne actuellement (%d connectés)',
            Object.keys(this.connected).length,
            Object.keys(logged).length
        );
    }
};

module.exports = Namespace;
