'use strict';

var express = require('express');
var path = require('path');

/**
 * Gestion des requêtes Web
 *
 * @param {Object} app Application Express
 * @return {null}
 */
module.exports = function (app) {
    var statics = path.join(__dirname, '/../public');
    
    app.use(express.static(statics));
    app.get('*', function (req, res) {
        res.sendFile(path.join(statics, 'index.html'));
    });
};
