'use strict';

/**
 * Choisit `amount` éléments du tableau `array`,
 * de manière aléatoire
 *
 * Adapté de :
 * http://sedition.com/perl/javascript-fy.html
 *
 * @param {Array} array Tableau à choisir
 * @param {number} amount Nombre d'éléments à choisir
 * @return {Array} Liste d'éléments choisis au hasard
 */
exports.pick = function (array, amount) {
    var i, random, swap;
    
    // copie le tableau
    array = [].slice.call(array);

    for (i = array.length - 1; i > 1; i -= 1) {
        random = Math.floor(Math.random() * i);
        swap = array[i];
        
        array[i] = array[random];
        array[random] = swap;
    }

    return array.slice(0, amount);
};
